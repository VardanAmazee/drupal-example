ARG CLI_IMAGE
FROM ${CLI_IMAGE} as cli

FROM amazeeio/php:7.2-fpm

RUN apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS openldap-dev && \
        docker-php-ext-install ldap && \
        apk del .phpize-deps && \
        rm -rf /tmp/* /var/cache/apk/*

COPY --from=cli /app /app
